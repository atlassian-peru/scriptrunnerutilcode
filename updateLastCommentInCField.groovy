import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.component.ComponentAccessor
import java.text.SimpleDateFormat
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder

// Get the required component
def customFieldManager = ComponentAccessor.customFieldManager

// the name of the custom field to update
final String lastCommentCustomFieldName = "Último Comentario"

def issueManager = ComponentAccessor.getIssueManager()
def commentManager = ComponentAccessor.getCommentManager()
String pattern = "dd/MM/yyyy hh:mm:ss";
SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

//def issue = issueManager.getIssueObject("{issuekey}")
def issue = issueManager.getIssueObject(event.issue.getKey())
def comment = commentManager.getLastComment (issue)
String createdDate = simpleDateFormat.format(comment.getCreated());
//" Autor: " + comment.authorFullName

String lastComment = "( " + createdDate + " ) " + comment.body

def lastCommentCustomField = customFieldManager.getCustomFieldObjects(issue).find { it.name == lastCommentCustomFieldName }
lastCommentCustomField.updateValue(null, issue, new ModifiedValue(issue.getCustomFieldValue(lastCommentCustomField), lastComment), new DefaultIssueChangeHolder())
lastComment