// The issue key
def issueKey = '<Colocar issuekey>'

// Specify the name of the select Request Language field to set
def rLanguageFieldName = 'Request language'

// Specify the name of the select list field to set
def cRLanguageFieldName = '<Campo que recibe valor>'

// Fetch the issue object from the key
def issue = get("/rest/api/2/issue/${issueKey}")
        .header('Content-Type', 'application/json')
        .asObject(Map)
        .body

// Get all the fields from the issue as a Map
def fields = issue.fields as Map

// Get the Custom field to get the option value from
def rLanguageCustomField = get("/rest/api/2/field")
        .asObject(List)
        .body
        .find {
    (it as Map).name == rLanguageFieldName
} as Map
// Check if the custom field returns a valid field and is not null
assert rLanguageCustomField != null : "Cannot find custom field with name of: ${rLanguageFieldName}"

// Get the Custom field to set the option value from
def cRLanguageCustomField = get("/rest/api/2/field")
        .asObject(List)
        .body
        .find {
    (it as Map).name == cRLanguageFieldName
} as Map
// Check if the custom field returns a valid field and is not null
assert cRLanguageCustomField != null : "Cannot find custom field with name of: ${cRLanguageFieldName}"

// Extract and store the option from the radio buttons custom field
def rLanguageValue = (fields[rLanguageCustomField.id] as Map).displayName


// Note: - Atlassian provide examples for the syntax to set other custom field types in the documentation page at https://developer.atlassian.com/server/jira/platform/jira-rest-api-examples/#creating-an-issue-examples
def result = put("/rest/api/2/issue/${issueKey}")
// Uncomment the line below if you want to set a field which is not pressent on the screen. Note - If using this you must run the script as the ScriptRunner Add-On User.
.queryString("overrideScreenSecurity", Boolean.TRUE)
.queryString("notifyUsers", Boolean.FALSE)
        .header('Content-Type', 'application/json')
        .body([
        fields: [
                (cRLanguageCustomField.id):[value: rLanguageValue] as Map
        ]
])
        .asString()

if (result.status == 204) {
    return "The ${cRLanguageCustomField.name} select list field was successfully updated on the ${issueKey} issue"
} else {
    return "${result.status}: ${result.body}"
}