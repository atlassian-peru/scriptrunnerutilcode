# README #

Este repositorio almacena scripts útiles que son ejecutados en Scriptrunner con determinado fin.

### Listado de Scripts ###

* _copyRequestRepository_ : Este script copia del campo bloqueado "Request Language" hacia un campo personalizado que condicionará sí se añadirá un comentario en el portal de clientes según el idioma de este.
* _updateLastCommentInCField_ : Este script copia el valor del último comentario ingresado en un campo personalizado.